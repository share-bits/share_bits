module.exports = {
  preset: 'react-native',
  transform: {
    '^.+\\.styl': '<rootDir>/node_modules/jest-css-modules-transform',
  },
  setupFiles: ['./__mocks__/jest.mock.js'],
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
};
