module.exports = {
  bracketSpacing: true, // bracket 의 공백 허용
  jsxBracketSameLine: true, //
  singleQuote: true, // 큰 따옴표 대신 작은 따옴표를 사용
  trailingComma: 'all', //
  printWidth: 80,
};
