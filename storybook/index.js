import { Navigation } from 'react-native-navigation';
import { getStorybookUI, configure } from '@storybook/react-native';
import { loadStories } from './loader';

// import './rn-addons';

// import stories
configure(() => loadStories(), module);

// Refer to https://github.com/storybookjs/storybook/tree/master/app/react-native#start-command-parameters
// To find allowed options for getStorybookUI
const StorybookUIRoot = getStorybookUI();

Navigation.registerComponent('storybook.UI', () => StorybookUIRoot);

Navigation.events().registerAppLaunchedListener(() => {
  return Navigation.setRoot({
    root: {
      component: {
        name: 'storybook.UI',
      },
    },
  });
});

export default StorybookUIRoot;
