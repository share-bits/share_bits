'use strict';

import './src/polyfills';

if (__DEV__ && process.env.STORYBOOK_UI === true) {
  require('./storybook');
} else {
  require('./src/initializeSystem');
}
