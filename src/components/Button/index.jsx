import Button from './Button';
import Touchable, { TouchablePropTypes, } from './Touchable';

export { Touchable, TouchablePropTypes };
export default Button;
