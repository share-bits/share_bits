// @flow
import React from 'react';
import {StyleSheet, Dimensions, Text, View, Image} from 'react-native';

import './styles.styl';

type GuideSceneProps = {
  primary: string;
  secondary?: string;
}

export default function GuideScene_3({primary, secondary}: GuideSceneProps) {
  return (
    <View styleName="GuideScene">
      <View styleName="GuideSceneContainer">
        <View styleName="GuideScenePrimary">
          <Text styleName="GuideScenePrimaryText">{primary}</Text>
        </View>
        <View styleName="compacted">
          <View style={[StyleSheet.absoluteFillObject, {overflow: 'hidden'}]}>
            <Image source={require('./guide-scene-3.png')} resizeMode="cover" style={{alignSelf: 'center'}} />
          </View>
        </View>
        <View styleName="GuideSceneBottom">
          <View styleName="GuideSceneSecondary pv10">
            <Text styleName="GuideSceneSecondaryText">{secondary}</Text>
          </View>
        </View>
      </View>
    </View>
  )
}
