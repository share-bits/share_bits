// @flow
import React from 'react';
import { StyleSheet, Text, TextProps } from 'react-native';

type ChromaTextPropTypes = TextProps & {
  value: number,
};
function ChromaTextComponent({
  value,
  style,
  ...textProps
}: ChromaTextPropTypes = {}) {
  let color,
    sign = Math.sign(value);

  if (isNaN(sign) || sign === 0) {
    color = '#9b9b9b';
  } else if (sign > 0) {
    color = '#F71B1E';
  } else {
    color = '#007AFF';
  }

  const textStyle = StyleSheet.flatten([{ color }, style]);
  return <Text style={textStyle} {...textProps} />;
}

export default React.memo(ChromaTextComponent);
