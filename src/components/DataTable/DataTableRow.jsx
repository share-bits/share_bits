/**
 * DataTable
 * DataTable.Row
 * DataTable.Cell
 *
 * @flow
 */

import React from 'react';
import { View } from 'react-native';

import styles from './styles';

export default function DataTableRow(props = {}) {
  const { children, style, ...viewProps } = props;
  const childrenCount = React.Children.count(children);

  return (
    <View {...viewProps} style={[styles.row, style]}>
      {React.Children.map(children, (child, index) => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {
            last: index === childrenCount - 1,
          });
        }
        return null;
      })}
    </View>
  );
}
