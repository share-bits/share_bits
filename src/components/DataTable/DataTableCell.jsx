/**
 * DataTable
 * DataTable.Row
 * DataTable.Cell
 *
 * @flow
 */

import React from 'react';
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native';

import styles from './styles';

type DataTableCellPropTypes = {
  width?: number,
  col?: number, // DEFAULT: 1
  center?: boolean,
  numeric?: boolean,
  title?: boolean,
  style?: StyleProp<ViewStyle>,
};
export default function DataTableCell(props: DataTableCellPropTypes = {}) {
  const {
    children,
    style,
    col,
    width,
    center,
    numeric,
    title,
    ...textProps
  } = props;

  let flex = parseInt(col, 10) || 1;
  if (width) {
    flex = 0;
  }

  const containerStyle = StyleSheet.flatten([
    styles.cell,
    { flex: flex > 12 ? 12 : flex },
    width && { width },
    center && { alignItems: 'center' },
    numeric && { alignItems: 'flex-end' },
    title && styles.title,
    style,
  ]);

  const CellElement = ['string', 'number'].includes(typeof children) ? (
    <Text numberOfLines={1} {...textProps}>
      {String(children)}
    </Text>
  ) : (
    children
  );

  return <View style={containerStyle}>{CellElement}</View>;
}
