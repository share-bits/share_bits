import React from 'react';
import { View, TextInput as BaseTextInput, Text } from 'react-native';
import styled from 'styled-components/native';

import type { TextInputProps, ViewStyle } from 'react-native';

const INPUT_HEIGHT = 42;
const INPUT_TEXT_COLOR = '#888888';
const INPUT_ICON_COLOR = 'gray';
const INPUT_LABEL_COLOR = '#333333';
const INPUT_FONT_SIZE = 16;
const INPUT_LABEL_FONT_SIZE = 14;
const INPUT_UNDERLINE_WIDTH = 1;
const INPUT_UNDERLINE_COLOR = 'rgb(238,238,238)';

/**
 *
 * @name Input
 * @property {number} height
 * @property {number} fontSize
 * @property {{[string]: any}} iconProps
 * @property {TextInputProps} inputProps
 * @property {ViewStyle} containerStyle
 */
type TextInputPropTypes = {
  height?: number,
  fontSize: ?number,
  iconProps?: {[string]: any},
  inputProps?: TextInputProps,
  containerStyle?: ViewStyle,
}
const TextInput = React.forwardRef((props: TextInputPropTypes, ref) => {
  const { containerStyle } = props
    , iconProps = props.iconProps || {}
    , inputProps = props.inputProps || {};

  if( !iconProps.color ) {
    iconProps.color = INPUT_ICON_COLOR;
  }

  delete inputProps.style;

  return (
    <TextInput.Container style={containerStyle}>
      {iconProps.icon && <TextInput.Icon {...iconProps} />}
      <TextInput.TextInput
        ref={ref}
        autoCapitalize="none"
        underlineColorAndroid="transparent"
        {...inputProps}
      />
    </TextInput.Container>
  )
});

TextInput.Container = styled(View)`
  flex-direction: row;
  align-items: center;
  
  padding: 0 5px;
  border-bottom-width: ${INPUT_UNDERLINE_WIDTH};
  border-bottom-color: ${INPUT_UNDERLINE_COLOR};
`;

TextInput.TextInput = styled(BaseTextInput)`
  flex: 1;
  height: ${props => props.height || INPUT_HEIGHT};
  font-size: ${props => props.fontSize || INPUT_FONT_SIZE};
  color: ${INPUT_TEXT_COLOR};
`;

TextInput.Label = styled(Text)`
  font-size: ${INPUT_LABEL_FONT_SIZE};
  color: ${INPUT_LABEL_COLOR};
  margin-bottom: 5px;
  font-weight: 700
`;

// TextInput.Icon = React.memo(({icon, ...iconProps}) => {
//   if( typeof icon === 'function' ) {
//     return icon.call(iconProps);
//   }
//   else if ( React.isValidElement(icon) ) {
//     return React.cloneElement(icon, iconProps);
//   }
//   else {
//     return React.cloneElement(<Icons name={icon} />, iconProps);
//   }
// });

export default TextInput;
