/**
 *
 * @flow
 */
import React from 'react';
import {
  View,
  Text,
  TextInput,
  TextInputProps,
  StyleProp,
  ViewStyle,
  StyleSheet,
} from 'react-native';

import styles from './styles.styl';

type InputPropTypes = TextInputProps & {
  height?: number,
  containerStyle?: StyleProp<ViewStyle>,
};
function InputComponent(props: InputPropTypes = {}) {
  const { height, containerStyle, style, ...inputProps } = props;
  const inputStyle = [styles.Input, style];

  return (
    <View style={[styles.Container, containerStyle]}>
      <TextInput
        underlineColorAndroid="transparent"
        {...inputProps}
        style={inputStyle}
      />
    </View>
  );
}

function InputLabel(props = {}) {
  const textStyle = StyleSheet.flatten([styles.InputLabel, props.style]);
  return <Text style={textStyle}>{props.children}</Text>;
}

const Input = React.forwardRef(InputComponent);

Input.Label = InputLabel;
Input.displayName = 'Input';

export default Input;
