// @flow
import React from 'react';
import {
  StyleSheet,
  StyleProp,
  Image,
  ImageSourcePropType,
  ImageStyle,
} from 'react-native';
import { useDimensions } from '@react-native-community/hooks';

type ResponsiveImagePropTypes = {
  width?: number,
  source: ImageSourcePropType,
  style?: StyleProp<ImageStyle>,
};
function ResponsiveImageComponent(props: ResponsiveImagePropTypes = {}) {
  const { window } = useDimensions();

  // 기준이 되는 폭
  const imageWidth: number = parseInt(props.width, 10) || window.width;
  const [imageHeight, setImageHeight] = React.useState(0);
  const [imageSource, setImageSource] = React.useState(null);

  React.useEffect(() => {
    const resolvedAssetSource = Image.resolveAssetSource(props.source);

    if (resolvedAssetSource.width && resolvedAssetSource.height) {
      setImageSource(resolvedAssetSource);
    } else {
      Image.getSize(resolvedAssetSource.uri, (originWidth, originHeight) => {
        setImageSource({
          ...resolvedAssetSource,
          width: originWidth,
          height: originHeight,
        });
      });
    }
  }, [props.source]);

  React.useEffect(() => {
    imageSource &&
      setImageHeight(
        Math.ceil((imageSource?.height / imageSource?.width) * imageWidth),
      );
  }, [imageSource, imageWidth]);

  //
  const imageStyle = StyleSheet.flatten(
    {
      width: imageWidth,
      height: imageHeight,
    },
    props.style,
  );

  return <Image source={imageSource} style={imageStyle} />;
}

export default React.memo(ResponsiveImageComponent);
