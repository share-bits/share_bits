import React from 'react';
import { View, Text } from 'react-native';
import styled from 'styled-components/native';

import type { ViewStyle, TextStyle } from 'react-native';

const ERROR_MESSAGE_COLOR = '#EF8B2F';
const ERROR_MESSAGE_HEIGHT = 26;
const ERROR_MESSAGE_FONT_SIZE = 13;

/**
 *
 * @name ErrorMessage
 * @property {string} message
 */
type ErrorMessagePropTypes = {
  message: string,
  textStyle?: TextStyle,
  containerStyle?: ViewStyle,
};
const ErrorMessage = React.memo((props: ErrorMessagePropTypes={}) => {
  const { containerStyle, textStyle } = props;

  return (
    <ErrorMessage.Container style={containerStyle}>
      <ErrorMessage.Text style={textStyle}>{props.message}</ErrorMessage.Text>
    </ErrorMessage.Container>
  )
});

ErrorMessage.Text = styled(Text)`
  font-size: ${ERROR_MESSAGE_FONT_SIZE};
  color: ${ERROR_MESSAGE_COLOR};
`;

ErrorMessage.Container = styled(View)`
  height: ${ERROR_MESSAGE_HEIGHT};
  justify-content: center;
  padding: 0 5px;
`;

export default ErrorMessage;
