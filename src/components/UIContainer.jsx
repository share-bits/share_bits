import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useInteractionManager } from '@react-native-community/hooks';

/**
 *
 * @param props
 */
export default function UIContainer(props={}) {
  const interactionReady = useInteractionManager();

  return (
    <View style={styles.container}>
      {interactionReady && props.children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  }
});
