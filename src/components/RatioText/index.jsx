import React from 'react';
import { Animated, View, Text } from 'react-native';

import useObjectState from 'utils/hooks/useObjectState';

import './styles.styl';

import type { TextStyle } from 'react-native';

const ORIGINAL_COLOR = '#FFFFFF';

type RatioTextPropTypes = {
  value: number,
  style?: TextStyle,
}
export default function RatioText(props: RatioTextPropTypes={}) {
  const animValue = React.useRef(new Animated.Value(0)).current;
  const [ state, setState ] = useObjectState({
    value: props.value,
    interactionReady: false,
    animateTintColor: ORIGINAL_COLOR,
    animating: false,
  });

  const sign = Math.sign(state.value);

  /**
   * 수익률 색상
   *
   * @type {string}
   */
  const textColor =
    isNaN(sign) || sign === 0
      ? '#9b9b9b'
      : sign > 0? '#f71b1e': '#1461df';

  React.useEffect(() => {
    if ( state.interactionReady ) {
      setState({
        value: props.value,
        animateTintColor: state.value > props.ratio? '#1461df': '#f71b1e'
      });

      if( state.animating === false ) {
        requestAnimationFrame(() => {
          setState({animating: true});

          Animated.timing(animValue, {
            duration: 250,
            toValue: 1,
          }).start(() => {
            Animated.timing(animValue, {
              duration: 250,
              toValue: 0,
            }).start(() => setState({animating: false}));
          });
        });
      }
    } else {
      setState({interactionReady: true});
    }

  }, [props.value]);

  const borderColor = animValue.interpolate({
    inputRange: [0, 1],
    outputRange: [ORIGINAL_COLOR, state.animateTintColor]
  });

  return (
    <View styleName="Ratio">
      <Animated.View style={{borderWidth: 1, borderColor, padding: 5,}}>
        <Text style={[{color: textColor}, props.style]}>
          {Number.percentage(state.value)}
        </Text>
      </Animated.View>
    </View>
  )
}
