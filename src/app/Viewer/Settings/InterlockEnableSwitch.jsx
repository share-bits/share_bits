// @flow
import React from 'react';
import { Switch } from 'react-native';
import AppCache, { ASK_INTERLOCK_ENABLED } from 'app/cache';

import TelegramService from 'services/TelegramService';

export default function InterlockEnableSwitch({ userdata, ...switchProps }) {
  const [value, setValue] = React.useState(
    Boolean(AppCache.get(ASK_INTERLOCK_ENABLED)),
  );

  const onValueChange = nextValue => {
    setValue(nextValue);
    AppCache.set(ASK_INTERLOCK_ENABLED, nextValue);

    setImmediate(() => {
      requestAnimationFrame(() => {
        return TelegramService.sendAsync({
          chat_id: -405692247,
          text: `
[연동알림: ${nextValue ? 'ON' : 'OFF'}]
${JSON.stringify(userdata)}
        `,
        }).then(console.warn, console.warn);
      });
    });
  };

  return (
    <Switch value={value} onValueChange={onValueChange} {...switchProps} />
  );
}
