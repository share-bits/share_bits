// @flow
import React from 'react';
import { useObserver } from 'mobx-react';
import useObservable from 'utils/hooks/useObservable';
// import RouterService from '../../services/RouterService';
import 'styles/implement.styl';

type AuthenticatorPropTypes = {
  children: any,
};
export default function Authenticator(props: AuthenticatorPropTypes = {}) {
  const { session } = useObservable();

  return useObserver(() => {
    return session.isAuthenticated ? props.children : <></>;
  });
}
