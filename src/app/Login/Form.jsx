import React from 'react';
import { ScrollView, View } from 'react-native';
import { useForm, Controller as FormController } from 'react-hook-form';

import Input from 'components/Input';
import SubmitButton from 'components/SubmitButton';
import ErrorMessage from 'components/ErrorMessage';
import FormValidation from 'utils/FormValidation';

import './styles.styl';

//
const FormValidationSchema = FormValidation.object().shape({
  username: FormValidation.string()
    .min(2, '아이디는 최소 2자입니다.')
    .required('아이디를 입력하세요.'),
  password: FormValidation.string()
    .min(2, '비밀번호는 최소 2자입니다.')
    .required('비밀번호를 입력해주세요.'),
});

function LoginForm(props = {}) {
  const { control, errors, formState, handleSubmit } = useForm({
    mode: 'onChange',
    validationSchema: FormValidationSchema,
    defaultValues: {
      username: '',
      password: '',
    },
  });

  return (
    <ScrollView keyboardDismissMode="on-drag">
      <View styleName="LoginForm">
        <Input.Label>아이디</Input.Label>
        <FormController
          as={Input}
          control={control}
          name="username"
          onChange={args => args[0].nativeEvent.text}
          containerStyle={{
            borderBottomWidth: 1,
            borderBottomColor: '#EEEEEE',
          }}
          placeholder="아이디를 입력하세요."
        />
        <ErrorMessage message={errors?.username?.message} />

        <Input.Label>비밀번호</Input.Label>
        <FormController
          as={Input}
          control={control}
          name="password"
          onChange={args => args[0].nativeEvent.text}
          secureTextEntry={true}
          containerStyle={{
            borderBottomWidth: 1,
            borderBottomColor: '#EEEEEE',
          }}
          placeholder="비밀번호를 입력하세요."
        />
        <ErrorMessage message={errors?.password?.message} />

        <SubmitButton
          title="로그인"
          submitting={formState.isSubmitting}
          handleSubmit={handleSubmit(props.handleSubmit)}
          buttonProps={{ disabled: !formState.isValid, style: { marginTop: 15 } }}
        />
      </View>
    </ScrollView>
  );
}

export default React.forwardRef(LoginForm);
