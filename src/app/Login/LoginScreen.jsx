// @flow
import React from 'react';
import { BackHandler, View, Alert, Platform, ToastAndroid } from 'react-native';
import { Navigation, ComponentEvent } from 'react-native-navigation';
import useObservable from 'utils/hooks/useObservable';
import LoginForm from './Form';
import './styles.styl';

type LoginScreenContainerPropTypes = ComponentEvent & {};
export function LoginScreenContainer(
  props: LoginScreenContainerPropTypes = {},
) {
  const { session } = useObservable();

  const handleSubmit = input => {
    session
      .createAsync(input.username, input.password)
      .then(() => import('app/setup').then(app => app.setup()))
      .catch(e => Alert.alert('알림', e.message));
  };

  return <LoginForm handleSubmit={handleSubmit} />;
}

/**
 *
 */
export default class LoginScreen extends React.Component {
  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);

    this.state = {
      appeared: false,
    };
  }

  componentDidAppear() {
    this.setState({ appeared: true });

    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPressed);
  }

  componentDidDisappear() {
    this.setState({ appeared: false });

    this.hardwareBackPressedTime = null;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.hardwareBackPressed,
    );
  }
  hardwareBackPressed() {
    const timestamp = new Date().getTime();
    if (
      this.hardwareBackPressedTime &&
      this.hardwareBackPressedTime + 3000 > timestamp
    ) {
      return false;
    }

    this.hardwareBackPressedTime = timestamp;
    ToastAndroid.show('한번 더 누르면 앱이 종료됩니다.', ToastAndroid.SHORT);
    return true;
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        {React.createElement(LoginScreenContainer, this.props)}
      </View>
    );
  }
}
