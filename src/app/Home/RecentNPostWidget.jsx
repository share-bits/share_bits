// @flow
import React from 'react';
import { FlatList, Image } from 'react-native';
import { useDimensions } from '@react-native-community/hooks';
import { useQuery } from 'react-query';
import urlParse from 'url-parse';

import { Touchable } from 'components/Button';
import RestClientService from 'services/RestClientService';
import RouterService from '../../services/RouterService';

const RESOURCE_CONFIG = {
  ITEM_MAX_WIDTH: 210,
  HORIZONTAL_MARGIN: 5,
  IMAGE_RATIO: 0.735,
  IN_APP_WEB_VIEW_HOST: ['CS.WLFA.US'],
};

type RecentNPostItemPropTypes = {
  title: string,
  image: string,
  created_at_str: string,
  created_at: string,
  link: string,
};

type RecentNPostWidgetItemRowPropTypes = {
  item: RecentNPostItemPropTypes,
  width: number,
  onPress: (item: RecentNPostItemPropTypes) => void,
};
const ItemRow = React.memo(
  ({ item, width, onPress }: RecentNPostWidgetItemRowPropTypes) => {
    const ITEM_WIDTH = width - 10;
    const containerStyle = { width, padding: 5 };
    const imageStyle = {
      width: ITEM_WIDTH,
      height: Math.ceil(ITEM_WIDTH * RESOURCE_CONFIG.IMAGE_RATIO),
      backgroundColor: '#CACACA',
    };

    return (
      <Touchable activeOpacity={0.9} style={containerStyle} onPress={onPress}>
        <Image
          source={{ uri: item.image }}
          resizeMode="cover"
          style={imageStyle}
        />
      </Touchable>
    );
  },
);

function queryFn(): Promise<[RecentNPostItemPropTypes]> {
  return RestClientService.get('http://sa.wlfa.us/recent-naver-post.json')
    .then(response => response.body)
    .then(({ data }) => data.items.slice(0, 20));
}

export default function RecentNPostWidget(props = {}) {
  const { window } = useDimensions();
  const { status, data } = useQuery('RecentNPostWidget', queryFn);

  const WINDOW_WIDTH = window.width - RESOURCE_CONFIG.HORIZONTAL_MARGIN * 2;
  const COLUMNS = Math.max(
    Math.round(WINDOW_WIDTH / RESOURCE_CONFIG.ITEM_MAX_WIDTH),
    2,
  );
  const ITEM_WIDTH = Math.ceil(WINDOW_WIDTH / COLUMNS);

  /**
   *
   * @param item
   */
  const onPressItem = (item: RecentNPostItemPropTypes) => {
    const hostname = urlParse(item.link).hostname;

    if (RESOURCE_CONFIG.IN_APP_WEB_VIEW_HOST.includes(hostname.toUpperCase())) {
      return RouterService.showWebViewModal({
        uri: item.link,
        title: item.title,
      });
    }

    return RouterService.openInAppBrowser(item.link);
  };

  const itemRenderFn = ({ item }) => (
    <ItemRow item={item} width={ITEM_WIDTH} onPress={() => onPressItem(item)} />
  );

  return (
    status === 'success' && (
      <FlatList
        data={data}
        key={JSON.stringify({ COLUMNS, WINDOW_WIDTH })}
        keyExtractor={item => item.title}
        renderItem={itemRenderFn}
        numColumns={COLUMNS}
        initialNumToRender={4}
        updateCellsBatchingPeriod={2}
        style={{ paddingHorizontal: RESOURCE_CONFIG.HORIZONTAL_MARGIN }}
      />
    )
  );
}
