/**
 *
 * @author martin<7fintech.co.kr>
 *
 * @flow
 */
import React from 'react';
import { View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { useForm, Controller as FormController } from 'react-hook-form';

import Input from 'components/Input';
import SubmitButton from 'components/SubmitButton';
import ErrorMessage from 'components/ErrorMessage';
import ContactsService from 'services/ContactsService';
import FormValidation from 'utils/FormValidation';

import './styles.styl';

//
const FormValidationSchema = FormValidation.object().shape({
  name: FormValidation.string()
    .min(2, '신청자 이름의 길이는 최소 2자입니다.')
    .required('신청자 이름을 입력하세요.'),
  cellphone: FormValidation.string()
    .cellphone('휴대전화번호 형식이 올바르지 않습니다.')
    .required('휴대전화번호를 입력해주세요.'),
});

type ContactUsPropTypes = {
  category?: string,
  componentId: string,
};
export default function ContactsForm(props: ContactUsPropTypes = {}) {
  const { control, errors, formState, handleSubmit } = useForm({
    mode: 'onBlur',
    validationSchema: FormValidationSchema,
    defaultValues: {
      name: '',
      cellphone: '',
      note: '',
    },
  });

  const onSubmit = input => {
    return ContactsService.createAsync({
      ...input,
      category: props.category || '직접신청',
    }).then(() => Navigation.dismissModal(props.componentId));
  };

  return (
    <View styleName="ContactsForm">
      <View styleName="pb25">
        <Input.Label>이름</Input.Label>
        <FormController
          as={Input}
          control={control}
          name="name"
          onChange={args => args[0].nativeEvent.text}
          defaultValue=""
          placeholder="신청자 이름을 입력해주세요."
          editable={formState.isSubmitting === false}
        />
        <ErrorMessage message={errors?.name?.message} />
      </View>
      <View styleName="pb25">
        <Input.Label>연락처</Input.Label>
        <FormController
          as={Input}
          control={control}
          name="cellphone"
          onChange={args => args[0].nativeEvent.text}
          defaultValue=""
          placeholder="연락받을 전화번호를 입력해주세요."
          editable={formState.isSubmitting === false}
        />
        <ErrorMessage message={errors?.cellphone?.message} />
      </View>
      <View styleName="pb25">
        <Input.Label>요청사항</Input.Label>
        <FormController
          as={Input}
          control={control}
          name="note"
          onChange={args => args[0].nativeEvent.text}
          defaultValue=""
          placeholder="요청사항 및 통화가능한 시간 등을 알려주세요."
          editable={formState.isSubmitting === false}
        />
        <ErrorMessage message={errors?.note?.message} />
      </View>

      <SubmitButton
        title="신청하기"
        submitting={formState.isSubmitting}
        handleSubmit={handleSubmit(onSubmit)}
        buttonProps={{ disabled: !formState.isValid }}
      />
    </View>
  );
}
