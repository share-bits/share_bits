// @flow
import React from 'react';
import {Platform} from 'react-native';
import GuideNewbie from 'components/GuideNewbie/GuideNewbie';

export default class GuideNewbieScreen extends React.Component {
  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  render() {
    return (
      <GuideNewbie {...this.props} />
    );
  }
}
