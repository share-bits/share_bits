// @flow
import React from 'react';
import { FlatList, FlatListProps } from 'react-native';

import RouterService from 'services/RouterService';
import useQueryManagers from 'utils/hooks/useQueryManagers';
import {
  ItemRow,
  ItemRowSeparator,
  ManagerPropTypes,
} from './ManagersPresenter';

import './styles.styl';

export type ManagersPropTypes = FlatListProps & {
  category: string,
  appeared?: boolean,
  refetchInterval?: number,
};

function onItemRowPressed(item: ManagerPropTypes) {
  return RouterService.showManagerModal({
    uri: 'https://thehantrader.com/cts_tab/cts_detail/' + item.page_code,
  });
}

function ManagersComponent(props: ManagersPropTypes = {}) {
  const {
    category,
    appeared,
    refetchInterval,
    ...flatListProps
  } = normalizeProps(props);

  const [refreshing, setRefreshing] = React.useState(false);
  const { status, data, refetch } = useQueryManagers(category, {
    refetchInterval: appeared ? refetchInterval : false,
  });

  return status === 'success' ? (
    <FlatList
      data={Array.isArray(data?.items) ? data.items : []}
      initialNumToRender={10}
      keyExtractor={item => item.page_code}
      showsVerticalScrollIndicator={false}
      refreshing={refreshing}
      onRefresh={() => {
        setRefreshing(true);
        return refetch().then(() => setRefreshing(false));
      }}
      renderItem={({ item }) => (
        <ItemRow
          item={item}
          buttonProps={{
            activeOpacity: 0.9,
            onPress: () => onItemRowPressed(item),
          }}
        />
      )}
      ItemSeparatorComponent={() => <ItemRowSeparator />}
      {...flatListProps}
    />
  ) : null;
}

/**
 *
 * @param props
 * @returns {ManagersPropTypes}
 */
function normalizeProps(props: ManagersPropTypes = {}): ManagersPropTypes {
  return {
    ...props,
    appeared: Boolean(props.appeared),
    refetchInterval: parseInt(props.refetchInterval, 10) || 5000,
  };
}

export default React.memo(ManagersComponent);
