/**
 *
 * @author Martin, Lee
 */
import { Alert } from 'react-native';
import TelegramService from './TelegramService';

const RESOURCE_CONFIG = {
  CHAT_ID: '-322940489',
  BOT_TOKEN: '879034252:AAH0OXV8XBwfTbNR4Daf_HOEvh_p0pYVP_U',
};

function toEscapeMessage(message) {
  return String(message)
    .replace(/\./g, '\\.')
    .replace(/_/g, '\\_')
    .replace(/\[/g, '\\[')
    .replace(/\(/g, '\\(')
    .replace(/\)/g, '\\)')
    .replace(/`/g, '\\`')
    .replace(/\*/g, '\\*');
}

class ContactsService {
  createAsync(params = {}) {
    const text = `
이름: ${params.name}
연락처: ${params.cellphone}
상품명: ${toEscapeMessage(params.category)}
유입경로: 더한트레이더앱
*요청사항*
${toEscapeMessage(params.note)}
  `;

    return TelegramService.sendAsync({
      chat_id: RESOURCE_CONFIG.CHAT_ID,
      text,
    })
      .then(response => {
        return Alert.alert(
          '알림',
          [
            params.name,
            '님 신청해주셔서 감사합니다.\n빠른 시간 내에 연락드리도록 하겠습니다.',
          ].join(''),
        );
      })
      .catch(e => {
        return Alert.alert(
          '에러 알림',
          '예상치 못한 오류가 발생했습니다. 문제가 계속 발생할 경우 고객센터에 문의주시기바랍니다.',
        );
      });
  }
}

export default new ContactsService();
