import { get } from 'lodash';
import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

@autobind
export class StockMasterService {
  queryAsync() {
    return RestClientService.get('/api/stock/master').then(response =>
      get(response.body, 'data'),
    );
  }
}

export default new StockMasterService();
