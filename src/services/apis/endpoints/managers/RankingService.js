import { get } from 'lodash';
import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

// https://thehantrader.com/api/cts_tab/manager_fetch_all
@autobind
export class RankingService {
  queryAsync(options) {
    return RestClientService.get('/api/thehanapp/trader/ranking', options).then(
      response => get(response.body, 'data'),
    );
  }
}

export default new RankingService();
