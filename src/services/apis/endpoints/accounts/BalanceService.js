import { get } from 'lodash';
import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

@autobind
export class BalanceService {
  getAsync(options) {
    return RestClientService.get('/api/user/balance/result', options).then(
      response => {
        return get(response.body, 'data');
      },
    );
  }

  queryAsync(options) {
    return RestClientService.get('/api/user/balance/list', options).then(
      response => {
        return get(response.body, 'data');
      },
    );
  }
}

export default new BalanceService();
