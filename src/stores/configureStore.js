import EnvStore from './env';
import SessionStore from './session';

export default function configureStore() {
  return EnvStore.hydrateAsync().then(() => SessionStore.hydrateAsync());
}
