/**
 * @flow
 */
import { autobind } from 'core-decorators';
import { asyncAction } from 'mobx-utils';

import { StockMasterService } from 'services/apis/endpoints/stock';

@autobind
export class EnvironmentModel {
  stockMaster = {};

  /**
   *
   * @param code
   * @returns {*}
   */
  getStockItem(code: string) {
    return this.stockMaster[code];
  }

  @asyncAction
  async *hydrateAsync() {
    const promises = [StockMasterService.queryAsync()];
    const responses = yield Promise.all(promises);

    // 종목마스터 정보를 받아온다.
    this.stockMaster = responses[0]?.items.reduce((acc, cur) => {
      acc[cur.stockcode] = cur;
      return acc;
    }, {});
  }
}

export default new EnvironmentModel();
