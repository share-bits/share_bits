'use strict';

import { autobind } from 'core-decorators';
import AsyncStorage from '@react-native-community/async-storage';

import invariant from 'invariant';
import { get, set, hasIn, isString, omit } from 'lodash';

import type { AsyncStorageStatic } from 'react-native';

/**
 * synchronized: boolean;
 * synchronizeAsync();
 * alias sync
 *
 * inMemoryCache.get(storedKey: string);
 * inMemoryCache.getItem
 * inMemoryCache.getAsync(storedKey: string);
 * inMemoryCache.set(storedKey: string, value: any, expiresTime: number);
 * inMemoryCache.setItem
 * inMemoryCache.setAsync(storedKey: string);
 * inMemoryCache.remove
 * inMemoryCache.removeItem
 * inMemoryCache.removeAsync(storedKey: string);
 *
 * inMemoryCache.clear
 * inMemoryCache.hasKey
 * inMemoryCache.hasKey
 *
 * payload: {
 *   data: any,
 *   expiresAt: number,
 *   createdAt: number,
 * }
 */

@autobind
export default class InMemoryCache {
  /**
   *
   */
  hydrate: boolean;
  /**
   *
   */
  storedPrefix: ?string;
  /**
   *
   */
  cache: AsyncStorageStatic;
  /**
   *
   */
  inMemoryCache: {[key: string]: string};
  /**
   *
   */
  syncPromise: Promise<void>;

  constructor() {
    this.cache = AsyncStorage;
    this.inMemoryCache = {};

    /**
     *
     */
    this.mergeOptions({hydrate: false, storedPrefix: null});
  }

  /**
   *
   * @param options
   */
  mergeOptions(options={}) {
    this.hydrate = get(options, 'hydrate', this.hydrate);
    this.storedPrefix = get(options, 'storedPrefix', this.storedPrefix);

    return this;
  }

  /**
   *
   * @param key
   * @returns {(string|null)|null}
   */
  get(key: string): string {
    if( this.hasKey(key) ) {
      return get(this.inMemoryCache, key) || null;
    }
    return null;
  }

  /**
   *
   * @param key
   * @returns {Promise<string | null>}
   */
  getAsync(key: string): Promise<string> {
    const storedKey = genKey(key, this.storedPrefix);
    return this.cache.getItem(storedKey);
  }

  /**
   *
   * @param key
   * @param value
   * @returns {InMemoryCache}
   */
  set(key: string, value: string): InMemoryCache {
    invariant(
      isString(value),
      'inMemoryCache.set(key: string, value: string): value 값은 문자열 값으로 선언되어야합니다.'
    );

    set(this.inMemoryCache, key, String(value));

    setImmediate(() => {
      if( this.hydrate === true ) {
        const storedKey = genKey(key, this.storedPrefix);
        return AsyncStorage.setItem(storedKey, value, (error) => {
          // Error 발생 시 해당 키를 삭제시킨다.
          if( error ) this.remove(key);
        });
      }
    });

    return this;
  }

  /**
   *
   * @param key
   * @param value
   * @returns {Promise<void>}
   */
  setAsync(key: string, value: string): Promise<any> {
    invariant(
      isString(value),
      'inMemoryCache.setAsync(key: string, value: string): value 값은 문자열 값으로 선언되어야합니다.'
    );

    const storedKey = genKey(key, this.storedPrefix);
    return this.cache.setItem(storedKey, String(value));
  }

  /**
   *
   * @param key
   * @returns {boolean}
   */
  hasKey(key: string): boolean {
    return hasIn(this.inMemoryCache, key);
  }

  /**
   *
   * @param key
   * @returns {InMemoryCache}
   */
  remove(key: string): InMemoryCache {
    const beforeRemoveCacheValue = this.get(key);

    this.inMemoryCache = omit(this.inMemoryCache, key);

    setImmediate(() => {
      if( this.hydrate ) {
        const storedKey = genKey(key, this.storedPrefix);
        return AsyncStorage.removeItem(storedKey, (error) => {
          if( error ) this.set(key, beforeRemoveCacheValue);
        });
      }
    });

    return this;
  }

  /**
   *
   * @param key
   * @returns {Promise<void>}
   */
  removeAsync(key: string): Promise<any> {
    const storedKey = genKey(key, this.storedPrefix);
    return this.cache.removeItem(storedKey);
  }

  /**
   *
   */
  clear() {
    this.inMemoryCache = Object.assign({});
  }

  /**
   *
   * @returns {Promise<void>}
   */
  sync() {
    return this.rehydrateAsync();
  }

  /**
   *
   * @returns {Promise<void>}
   */
  rehydrateAsync() {
    if( false === this.hydrate ) {
      return Promise.resolve(null);
    }

    if( !this.syncPromise ) {
      const STORED_PREFIX = this.storedPrefix, vm = this;

      this.syncPromise = new Promise((resolve, reject) => {
        return this.cache.getAllKeys((errorKeys, keys) => {
          if( errorKeys ) return reject(errorKeys);

          const memoryKeys = keys.filter((key) => key.startsWith(STORED_PREFIX));
          return this.cache.multiGet(memoryKeys, (error, stores) => {
            if (error) reject(error);

            stores.map(([key, value]) => {
              const memoryKey = key.replace(STORED_PREFIX, '');
              //
              set(this.inMemoryCache, memoryKey, value);
            });

            resolve(vm);
          });
        })
      });
    }

    return this.syncPromise;
  }
}

function genKey(key: string, prefix: ?string=null) {
  if( typeof prefix === 'string' ) {
    return `${prefix}${key}`;
  }
  return key;
}
