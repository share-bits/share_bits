import { isNumber } from 'lodash';

export default function stubNumber(origin=null, placeholder=0) {
  if( isNumber(origin) ) return origin;
  return placeholder;
}
