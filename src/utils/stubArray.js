import { isArray } from 'lodash';

export default function stubArray(origin=null, placeholder=[]) {
  if( isArray(origin) ) return origin;
  return placeholder;
}
