(function () {
  'use strict';

  if( typeof Number.percentage == 'function' ) {
    return;
  }

  const numeral = require('numeral')
    , isNumber = require('lodash/isNumber');

  /**
   *
   * @param input
   * @param decimals
   * @return {string}
   */
  function percentage(input: number, decimals: number=2) {
    const num = numeral(input), unit = '%';

    const outcome =
      isNumber(decimals) && decimals > 0
        ? num.format(`0,0.${'0'.repeat(decimals)}`)
        : num.format(`0,0`);

    return `${outcome}${unit}`;
  }

  Number.percentage = percentage;
})();
