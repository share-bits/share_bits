(function() {
  'use strict';

  if (typeof Number.currency === 'function') {
    return;
  }

  const numeral = require('numeral');
  const CURRENCY_KRW = '원';
  const CURRENCY_USD = '$';

  type CurrencyType = 'KRW' | 'USD';

  /**
   *
   * @param input
   * @param currency
   * @return {*}
   */
  function currency(input: number, currency: ?CurrencyType = 'KRW') {
    const num = numeral(input);

    switch (currency) {
      case 'KRW':
        return `${num.format('0,0')}${CURRENCY_KRW}`;
      case 'USD':
        return num.format(`${CURRENCY_USD}0,0.00`);
      default:
        return num.format('0,0');
    }
  }

  Number.currency = currency;
})();
